package com.visitor.models;

import lombok.Data;


@Data
public class Transactions{
    
    private int id;

    private String punchTime;
}