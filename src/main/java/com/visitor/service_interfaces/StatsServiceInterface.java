package com.visitor.service_interfaces;

import java.util.List;

import com.visitor.entities.Transactions;

public interface StatsServiceInterface {
    public List<Transactions> getListPunch();
}
